<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;

class UploadController extends Controller {
	
	public function upload() {
		// getting all of the post data
		
		
		$file = array (
				'json' => Input::file ( 'json' ) ,
				'csv' => Input::file ( 'csv' )
		);
		// setting up rules
		$rules = array (
				'json' => 'required',
				'csv' => 'required'
		); // mimes:jpeg,bmp,png and for max size max:10000
		   // doing the validation, passing post data, rules and the messages
		$validator = Validator::make ( $file, $rules );
		if ($validator->fails ()) {
			// send back to the page with the input data and errors
			return response()->json( array(
						"name"=>'text-danger', 'response' =>'Please upload csv and json files both needed' )
						);
		} else {
			// checking file is valid.
			if (Input::file ( 'json' )->isValid ()) {
				$destinationPath = 'uploads'; // upload path
				$extension = Input::file ( 'json' )->getClientOriginalExtension (); // getting json extension
				$fileName =  'json.' . $extension; // renameing json
				$file = Input::file ( 'json' )->move ( $destinationPath, $fileName ); // uploading file to given path
				$jsonText = file_get_contents ( $file );
				
				$json = json_decode ( $jsonText );
			} else {
				// sending back with error message.
				return response()->json( array(
						"name"=>'text-danger', 'response' => 'uploaded file is not valid' )
						);
				
				//return Redirect::to ( 'upload' );
			}
			
			if (Input::file ( 'csv' )->isValid ()) {
				$csv_meta = array (
						"item",
						"amount",
						"unit",
						"useby" 
				);
				$csvData = array ();
				$first_col = true;
				$destinationPath = 'uploads'; // upload path
				$extension = Input::file ( 'csv' )->getClientOriginalExtension (); // getting csv extension
				$fileName =  'csv' . $extension; // renameing csv
				$file = Input::file ( 'csv' )->move ( $destinationPath, $fileName ); // uploading file to given path
				$jsonText = file_get_contents ( $file );
				
				$file_handle = fopen ( $file, 'r' );
				while ( $csv_line = fgetcsv ( $file_handle, 1024 ) ) {
					if ($first_col) {
						$first_col = false;
						continue;
					}
					
					$csv = array (
							"item" => '',
							"amount" => '',
							"unit" => '',
							"useby" => '' 
					);
					
					for($i = 0, $j = count ( $csv_line ); $i < $j; $i ++) {
						$csv [$csv_meta [$i]] = $csv_line [$i];
					}
					;
					$csvData [] = $csv;
				}
				fclose ( $file_handle );
				;
				$return_item = array (
						'name' => '',
						"ingamount" => 0,
						"invamount" => 0 
				);
				$data = array();
				foreach ( $csvData as $fridge ) {
					
					foreach ( $json as $item ) {
						foreach ( $item->ingredients as $ingredient ) {
							list($date,$month,$year) = sscanf($fridge ['useby'], "%d/%d/%d");
							//echo "$month-$date-$year";
							//return response()->json(array("$month-$date-$year"));
							$expire_date = date ( 'Y-m-d', strtotime (date("$year-$month-$date")) );
							$now = date ( 'Y-m-d', strtotime ( "now" ) );
							if (strtotime ( $now ) > strtotime ( $expire_date )) {
								$data [] = array($expire_date, strtotime ( $expire_date ),$now,strtotime ( $now ) );
								continue;
							}
							if ($fridge ['item'] == $ingredient->item && $fridge ['amount'] >= $ingredient->amount) {
								if ($fridge ['amount'] > $return_item ['invamount']) {
									$return_item ['invamount'] = $fridge ['amount'];
									$return_item ['ingamount'] = $ingredient->amount;
									$return_item ['name'] = $item->name;
								}
							}
						}
					}
				}
				if (empty ( $return_item ['name'] )) {
					return response()->json($data);return;
					$return_item ['name'] = "Order Takeout";
				}
				return response()->json( array ( 'name'=>'text-success','response' =>  $return_item ['name'] ));
				
			} 

			else {
				// sending back with error message.
				return response()->json( array ( "name"=>'text-danger', 'response' => 'uploaded file is not valid' ));
				
				//return Redirect::to ( 'upload' );
			}
		}
	}
}