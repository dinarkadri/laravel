<!DOCTYPE html>
<html>
<head>
<title>Laravel Test</title>
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.min.js"></script>
</head>

<body ng-app="fupApp" class="container">
<h1> Please upload fridge.csv and recipes.json </h1>

	<div ng-controller="fupController" class="container">
<h2 class="<% response.name %>"> <% response.response %></h2>
		<input type="file" id="file" name="file" class="form-control jumbotron" multiple
			onchange="angular.element(this).scope().getFileDetails(this)" /> <input
			type="button" ng-click="uploadFiles()" value="Upload" class="btn btn-lg btn-primary btn-block" />
	</div>
	
	

</body>

<script>
    var myApp = angular.module('fupApp', [], function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
    });
    myApp.controller('fupController', function ($scope) {
    	$scope.response = {"name": "test", "response": "Please upload"};//obj.response;
        // GET THE FILE INFORMATION.
        $scope.getFileDetails = function (e) {

            $scope.files = [];
            $scope.$apply(function () {

                // STORE THE FILE OBJECT IN AN ARRAY.
                for (var i = 0; i < e.files.length; i++) {
                    $scope.files.push(e.files[i])
                }

            });
        };
        var reply = $scope;
        
        // NOW UPLOAD THE FILES.
        $scope.uploadFiles = function () {

            //FILL FormData WITH FILE DETAILS.
            var data = new FormData();

            for (var i in $scope.files) {
            	var inname = $scope.files[i].name.split(".")
                 data.append(inname[1], $scope.files[i]);
               
            }
            data.append("_token","{{ csrf_token() }}");
            data.append("_method","PATCH");
            // ADD LISTENERS.
            var objXhr = new XMLHttpRequest();
            
            // SEND FILE DETAILS TO THE API.
            objXhr.open("POST", "upload/add");

            objXhr.onreadystatechange=function() {
            	  if (objXhr.readyState==4) {
            	   //alert the user that a response now exists in the responseTest property.
            	 
            	   obj = JSON.parse(objXhr.responseText);
            	   $scope.$applyAsync(function(){
            		   $scope.response=obj;
            		});
                   //reply.response = "testing";//obj.response;
            	  }
            }
            	  objXhr.send(data);
           
        }
  
        
    });
</script>

</body>
</html>